# RX Watches

RX Watches was a demo for a shopping site that I was working on.
The focus was a minimalist menu and navigation, while also having a pop-up design meaning that the site plays a lot with the depth of the elements going behind and on top of others without interrupting the flow of the experience.

<a href="https://virtualclover.com.mx/web/RX/hosted/index.html"><b>Check out the live demo!</b></a>