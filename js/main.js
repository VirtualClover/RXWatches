/*============================

RX Watches
design by Guillermo López
virtualclover.com.mx

===============================*/
var items = 0;

var magnus = function(){
    
    return[ $(".prod-text h1").text('MAGNUS'), $(".prod-img img").attr("src",'img/product/magnus.jpg'), $('.color1').css('background-color','#649f98').attr('onclick','aqua()'), $('.color2').css('background-color','black').attr('onclick','dark()')];
                       };
var aqua = function(){
    
    return[ $(".prod-text h1").text('AQUA'), $(".prod-img img").attr("src",'img/product/aqua.jpg'), $('.color1').css('background-color','#D9B179').attr('onclick','magnus()'), $('.color2').css('background-color','black').attr('onclick','dark()')];
                       };
var dark = function(){
    
    return[ $(".prod-text h1").text('DARK'), $(".prod-img img").attr("src",'img/product/dark.jpg'), $('.color1').css('background-color','#649f98').attr('onclick','aqua()'), $('.color2').css('background-color','#D9B179').attr('onclick','magnus()')];
                       };

/*Preloader*/

$(window).on('load', function()  {
    
	$('.preloader').css('opacity','0');
    $('body').css('overflow','auto');
    $('.half-image').css({'opacity': '1','top': '0'});
    
    setTimeout( function(){
    $('.half-text').css('opacity','1');
    $('.page-body').css({'opacity': '1','top': '-70px'});
    $('.w-logo').delay(500).fadeOut(500);
    $('.w-text').delay(1000).fadeIn(500);
    $('.banner-logo').delay(1000).fadeIn(500);    
    },800);
    
    $('.preloader').remove();
     var position = $(window).scrollTop(); 
            var banner= 1;
             if (banner < position)
                {
                    $(".page-body").removeClass("gold");
                } else {
                    
                    $(".page-body").addClass("gold");
                }
    
});

/*----------------Menu----------------------*/


 $('.menu-btn').click(function(){
     
     $('nav').toggleClass('expand');
     
     $('.s-bag').toggleClass('expand');
     
     
     
 });

 $('.s-bag').click(function(){
     
     $('nav').toggleClass('expand');
     
     $('.s-bag').toggleClass('expand');
     
     
     
 });


$(window).scroll(function() {
            var position = $(window).scrollTop(); 
            var banner= 1;
             if (banner < position)
                {
                    $(".page-body").removeClass("gold");
                    $('.menu-titles').css('opacity','0');
                } else {
                    
                    $(".page-body").addClass("gold");
                    $('.menu-titles').css('opacity','1');
                }
            
}); /*About section color change in scroll*/

$('nav a[href*="#"]').on('click', function(e) {
  e.preventDefault()
    
    $('body').css('overflow-y','auto');
    $('main').removeClass('shop');
    $('#product-cont').removeClass('shop');     
    $('html, body').animate(
    {
      scrollTop: $($(this).attr('href')).offset().top-70,
    },
    500,
    'linear'
  )
});

$('.arrow').on('click', function(e) {
  

  $('html, body').animate(
    {
      scrollTop: $("#about").offset().top,
    },
    400,
    'linear'
  )
});

$('body').on('click','.sb-x', function(e){
    
    $(this).css('display','none');
    $(this).parent().addClass('remove').removeClass;
    
    setTimeout( function(){
    $('.remove').remove();
    items  = parseInt(items) - 1;
    $('.s-bag p').text(items);
    
    if (items < 1){
            
            $('.none-items').css('display', 'block');
            $('.s-bag p').css('opacity','0');
             $('.check-btn').css('display', 'none');
            $('.no-items').css('display','table');
            $('.co-content').css('display','none');
        }           
        
               
               
    },250);
    
    
    
    
    
    
       
    
});

/*-------------------Order button and the product window-----------------*/

$('.watch').click(function(){
    
 $('body').css('overflow-y','hidden');
 $('main').addClass('shop');
 $('#product-cont').addClass('shop');
 $("#w-type[name=tier]").val("");

});

$('.order-btn').click(function(){
    
    $(".prod-img .main-img").attr("src","img/product/magnus.jpg");
    $(".prod-text h1").text("MAGNUS");
    $('body').css('overflow-y','hidden');
    $('main').addClass('shop');
    $('#product-cont').addClass('shop');
    $("#w-type[name=tier]").val("");
});

$('.prod-close a').click(function(){
                       
    $('body').css('overflow-y','auto');
    $('main').removeClass('shop');
    $('#product-cont').removeClass('shop');  
                       
                       
                       });

$("#w-type[name=tier]").change(function(){
            
});


$(".add-btn").click(function(){
    
    if ($("#w-type[name=tier]").val() !== "") {
        
    var image = $('.prod-img .main-img').attr('src');
    var title = $('.prod-text h1').text();
    $("#w-type option:selected").text();
    items  = parseInt(items)+1;
    $('.sb-inner').prepend('<div class="sb-item new"><div class="sb-x"><p>&times;</p></div><img src=' + image + '><p>' + title + '</p><p class="tier"></p></div>');
    $('.s-bag').css('opacity', '1');
    $('.s-bag p').text(items).css('opacity', '1');
     setTimeout( function(){
         $('.new .tier').text($("#w-type[name=tier]").val());
        $('.new').css({'width': '80%','height': 'auto'}).removeClass('new');},50);
        
        if (items > 0){
            
            $('.none-items').css('display', 'none');
            $('.check-btn').css('display', 'block');
        }
    }
});


